from streetkak import StreetkakScraper
scraper = StreetkakScraper()
print "Starting Scraper..."
errormsg = scraper.startscrape()
if errormsg:
	print "Got error, exiting:" + errormsg
else:
	data = scraper.getdata()
	for item in data:
		print "Truck Name: " + 		item["name"]
		print "Truck Location: " + 	item["address"]
		print "Opening Time:" + 	item["checkInTime"]
		print "Closing Time: " + 	item["checkOutTime"]
		print "Lat/Lon: %s/%s"% 	(item["lat"],item["lon"])
		print ""

print "\nPress Enter to exit..."
raw_input() 
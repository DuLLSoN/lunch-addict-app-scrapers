from bookthattruck import BookthattruckScraper

scraper = BookthattruckScraper()
error = scraper.startscrape()
if error:
	print "\n Got error, ending"
	exit()

datalist = scraper.getdata()
for data in datalist:
	print "Calendar related email: " + data["scr"]
	print "Calendar title location: "+ data["title"]
	print "Full location from the box above calendar frame:" + data["fullLocation"]
	print "Sync token: " + data["nextSyncToken"]
	print "Retrieved events:"
	for event in data["events"]:
		print "\t[Opening: %s][Closing: %s] - Detail: %s"% (event["start"],event["end"],event["summary"])
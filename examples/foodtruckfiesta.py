from foodtruckfiesta import FoodtruckfiestaScraper

scraper = FoodtruckfiestaScraper()
error = scraper.startscrape()

if error:
	exit()

#Items from Foodtruckfiesta map:
maptrucklist = scraper.getdata()
for item in maptrucklist:
	print "Name: %s" % item["name"]
	print "Lat: %s" % item["lat"]
	print "Lon: %s" % item["lon"]
	print "Details page: %s" % item["truckpage"]
	print "Time: %s" % item["time"]
	print "Tweet: %s" % item["tweet"]
	print "=========="

#getting details of all active trucks on foodtruckfiesta
detailstrucklist = scraper.scrapetrucklist()
for item in detailstrucklist:
	print item["name"]
	print item["truckpage"]
	if "twitter" in item:
		print "twitter: " + item["twitter"]
	if "facebook" in item:
		print "facebook: " + item["facebook"]
	if "website" in item:
		print "website: " + item["website"]
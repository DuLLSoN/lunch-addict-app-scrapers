import random, math, sys, datetime
import requests
import re
import json

class StreetkakScraper(object):
    """Scraper for streetkak.se"""
    exc_info=None
    data=[]
    defaultheaders= {
        "Connection": "keep-alive",
        "Origin": "http://streetkak.se",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36",
        "Content-Type": "text/plain",
        "Accept": "*/*",
        "Referer": "http://streetkak.se/",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US;q=0.6,en;q=0.4"}
    defaultjson = {
        "_ApplicationId": "",
        "_ClientVersion": "js1.2.18", #TODO: extract this from http://streetkak.se/js/parse-1.2.18.min.js
        "_InstallationId": "",
        "_JavaScriptKey": "",
        "_method": "GET",
        "limit": 1000,
        "order": "order"}

    def startscrape(self):
        try:
            self.getApiKeys()

            jsonobject = self.defaultjson.copy()
            jsonobject.update({
                "order": "checkInTime",
                "where": {
                    "checkOutTime": {
                        "$gt": {
                            "__type": "Date",
                            "iso": datetime.datetime.utcnow().isoformat()[:-3]+'Z'
                        }
                    }
                }})

            count = self.getItemCount(jsonobject=jsonobject)
            truckdata = self.getTruck()

            for x in xrange(0,count,1000):
                checkindata = self.getCheckIn(skip=x, jsonobject=jsonobject)
                self.parseData(checkindata, truckdata)
        except Exception, e:
            if self.exc_info:
                import traceback
                return traceback.print_exception(*self.exc_info)  
            else:
                return e





    def getApiKeys(self):
        try:
            r = requests.get("http://streetkak.se/js/truck.js", headers=self.defaultheaders)
        except requests.exceptions.RequestException, e:
            self.exc_info = sys.exc_info()
            raise e

        try:
            keymatch = re.search(r'Parse\.initialize\("(?P<AppID>.+?)","(?P<JSK>.+?)"\)', r.text)
            if not keymatch:
                raise ValueError("truck.js has no keys to parse")
        except Exception, e:
            self.exc_info = sys.exc_info()
            raise e
        else:
            AID = keymatch.group("AppID")
            JSK = keymatch.group("JSK")

        #_InstallationId algorithm from javascript
        #c = Math.floor(65536 * (1 + Math.random()) ).toString(16).substring(1)
        #b._installationId = c() + c() + "-" + c() + "-" + c() + "-" + c() + "-" + c() + c() + c()
        c = lambda: ("%x"% math.floor(65536 * (1 + random.random())))[1:]
        IID = c() + c() + "-" + c() + "-" + c() + "-" + c() + "-" + c() + c() + c()
        self.defaultjson.update({
        "_ApplicationId": AID,
        "_InstallationId": IID,
        "_JavaScriptKey": JSK,
            })

    def postRequest(self, url, jsonobject):
        try:
            r = requests.post(url, headers=self.defaultheaders, data = json.dumps(jsonobject))
            return r.json()
        except requests.exceptions.RequestException, e:
            self.exc_info = sys.exc_info()
            raise e

    def getItemCount(self, jsonobject=None):
        jsonobj = jsonobject.copy() if jsonobject else  self.defaultjson.copy()
        jsonobj.update({
            "count":1,
            "limit":0
            })
        responsejson = self.postRequest("https://api.parse.com/1/classes/CheckIn", jsonobj)
        try:
            count = int(responsejson["count"])
            return count
        except ValueError, e:
            self.exc_info = sys.exc_info()
            raise e

    def getCheckIn(self, skip=0, jsonobject=None):
        jsonobj = jsonobject.copy() if jsonobject else  self.defaultjson.copy()
        jsonobj.update({
            "skip": skip
            })
        return self.postRequest("https://api.parse.com/1/classes/CheckIn", jsonobj)

    def getTruck(self):
        jsonobject = self.defaultjson.copy()
        jsonobject.update({
            "keys": "name,isActive"
            })

        return self.postRequest("https://api.parse.com/1/classes/Truck", jsonobject)

    def parseData(self, checkindata, truckdata):
        for checkinitem in checkindata["results"]:
            try:
                saveditem = {
                "address": checkinitem.get("address", None),
                "checkInTime": checkinitem["checkInTime"]["iso"],
                "checkOutTime": checkinitem["checkOutTime"]["iso"],
                "lat": checkinitem["location"]["latitude"],
                "lon": checkinitem["location"]["longitude"],
                "name":""
                }
                for truckitem in truckdata["results"]:
                    if checkinitem["truckId"] == truckitem["objectId"]:
                        saveditem["name"] = truckitem["name"]
                        break
                self.data.append(saveditem)
            except Exception, e:
                e.args = e.args + json.dumps(checkinitem)
                self.exc_info = sys.exc_info()
                raise e

    def getdata(self):
        if self.data:
            return self.data
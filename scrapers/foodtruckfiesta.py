import re
import HTMLParser

import gevent
import gevent.monkey
gevent.monkey.patch_all()
from gevent.pool import Pool
import requests
from lxml import html

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)

class FoodtruckfiestaScraper(object):
	"""docstring for FoodtruckfiestaScraper"""

	data=[]

	def __init__(self):
		self.log = logging.getLogger(type(self).__name__)

	def getdata(self):
		if self.data:
			return self.data

	def startscrape(self):
		try:
			trucklist = self.getTrucksLoc("http://foodtruckfiesta.com/apps/map_json.php")
			trucklist2 = self.getTrucksLoc("http://foodtruckfiesta.com/apps/ncdmap_json.php")
			self.data = trucklist + trucklist2
			return None
		except Exception, e:
			self.log.exception("Unhandled exception:")
			return e
		
	def scrapetrucklist(self):
		trucklist = self.getTruckActiveList()
		pool = Pool(10)

		def worker(url):
				details = self.getTruckDetails(url)
				details["truckpage"] = url
				return details

		detailedtrucklist = pool.map(worker,trucklist)
		return detailedtrucklist

	def getTrucksLoc(self, url):
		r = requests.get(url)
		jobj = r.json()
		trucklist = []
		for item in jobj["markers"]:
			truckitem = self.parseTruckItem(item)
			if truckitem: 
				trucklist.append(truckitem)
		return trucklist

	def parseTruckItem(self, item):
		def checkregex(exp, subject):
			match = re.search(exp, subject)
			if match:
				h = HTMLParser.HTMLParser()
				return h.unescape(match.group(1))
			else:
				return ""
		try:
			truckitem={}
			htmlcontent = item["alerttext"]
			truckitem["name"] = item.get("print_name").encode('utf-8') or checkregex("<a.*?>(.+?)</a>(?!</font>)", htmlcontent)
			truckitem["lat"] = item.get("coord_lat")
			truckitem["lon"] = item.get("coord_long")
			truckitem["truckpage"] = checkregex('href="(http://foodtruckfiesta.com/.+?)"', htmlcontent)
			truckitem["time"] = checkregex("<b>Time:</b>(.+?)<", htmlcontent)
			truckitem["tweet"] = checkregex("<b>Tweet:</b>(.+?)<", htmlcontent).encode('utf-8')
			return truckitem
		except KeyError, e:
				self.log.exception("Error while parsing this item: %s", item)


	def getTruckActiveList(self):
		self.log.info("Getting all active trucks...")
		r = requests.get("http://foodtruckfiesta.com/")
		tree = html.fromstring(r.text)
		truckurllist=[]
		for url in tree.xpath("//ul[contains(@class,'active_truck_list')]/li/a/@href"):
			url = url if url.endswith("/") else url+"/"
			truckurllist.append(url)
		self.log.info("Found %d active trucks total.",len(truckurllist))
		return truckurllist

	def getTruckDetails(self, url):
		self.log.info("Getting truck details from: %s", url)
		r = requests.get(url)
		tree = html.fromstring(r.text)
		truckinfo ={}
		try:
			truckinfo["name"] = tree.xpath("//div[@class='post-title']/a")[0].text.replace(u'\u2019',"'").encode('utf-8')
			for node in tree.xpath("//div[@class='post-content']/h2[text()='>> ']/a"):
				for service in ["Twitter", "Facebook", "Website"]:
					if node.text.endswith(service):
						truckinfo[service.lower()]= node.attrib["href"]
						break
		except AttributeError, e:
			self.log.exception("Error on page: %s",url)
		self.log.debug("Parsed: %s / %s",truckinfo["name"], ", ".join(truckinfo.keys()))
		return truckinfo
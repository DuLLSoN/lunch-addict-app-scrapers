import requests
import re
import urllib
import datetime
from lxml import html

import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger("requests").setLevel(logging.WARNING)




class BookthattruckScraper(object):
	"""docstring for BookthattruckScraper"""
	data=[]
	APIKEY = "AIzaSyBNlYH01_9Hc5S1J9vuFmu2nUqBZJNAXxs"

	def __init__(self):
		self.log = logging.getLogger(type(self).__name__)
			

	def startscrape(self):
		self.log.info("Started scraping...")
		try:
			placedata = self.getPlaces()
			for item in placedata:
				result = self.getEventsAPI(calendarId= item["src"])
				dataitem = {
					"scr": item["src"],
					"title": item["title"],
					"fullLocation": item["location"],
					"events": result["eventList"],
					"nextSyncToken": result["nextSyncToken"]}
				self.data.append(dataitem)
			self.log.info("Finished scraping with success.")
		except Exception, e:
			self.log.exception("Scraping is unsuccessful, exiting.")
			return e
		
		
	def getPlaces(self):
		"""gets location(calendar title) and mail addresses from calendar page"""
		url = "http://www.bookthattruck.com/calendar.html"
		self.log.info("Downloading %s", url)
		r = requests.get(url)
		self.log.info("Parsing locations...")

		placedata=[]
		for match in re.finditer('src=".+?/calendar/embed.+?title=(?P<title>.+?)&.*?src=(?P<src>.+?)&.*?"', r.text):
			placeitem = {
				"title": urllib.unquote(match.group("title")), 
				"src":   urllib.unquote(match.group("src"))}
			placedata.append(placeitem)

		loclist=[]
		tree = html.fromstring(r.text)
		for node in tree.xpath("//div[@class='txt ' and h2]"):
			loc = (" ".join(node.xpath(".//strong//text()"))).replace(" .","").replace("  "," ").replace(u'\xa0', u' ')
			loclist.append(loc)

		for i, item in enumerate(placedata):
			item["location"]=loclist[i]

		self.log.info("%d locations parsed.", len(placedata))
		return placedata


	def getEventsAPI(self, calendarId, sync=None):
		"""gets calendar events from google calendar api"""
		calendarItems = []
		requestdata = {
			"calendarId": calendarId,
			"singleEvents": "true",
			"fields": "items(summary,start,end),nextPageToken,nextSyncToken,summary",
			"maxResults": 2500,
			"timeMin": datetime.datetime.utcnow().isoformat()[:-3]+'Z',
			"key": self.APIKEY
			}

		if sync:
			requestdata["syncToken"] = sync

		self.log.info("Getting Eventlist for %s...",calendarId)

		url = "https://clients6.google.com/calendar/v3/calendars/%s/events"%calendarId
		while True:
			r = requests.get(url, params=requestdata)
			if r.status_code is 410:
				return getCalendar(calendarId)
			jsonobject = r.json()

			for item in jsonobject["items"]:
				try:
					newitem = {
						"start": item["start"]["dateTime"],
						"end": item["end"]["dateTime"],
						"summary": item["summary"]
						}
					
				except KeyError, e:
					self.log.exception("Current item: %s", item)
				else:
					calendarItems.append(newitem)

			self.log.info("%3d items scraped for %s", len(calendarItems), calendarId)

			if "nextPageToken" in jsonobject:
				requestdata["pageToken"] = jsonobject["nextPageToken"]
			else:
				break

		return {
			"nextSyncToken": jsonobject["nextSyncToken"],
			"eventList": calendarItems
		}


	def getdata(self):
		if self.data:
			return self.data
